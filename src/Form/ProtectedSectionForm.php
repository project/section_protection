<?php

namespace Drupal\section_protection\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Class ProtectedSectionForm.
 */
class ProtectedSectionForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $protected_section = $this->entity;
    $section_uids = !empty($protected_section->get('section_users')) ? array_column($protected_section->get('section_users'), 'target_id') : NULL;
    $section_users = !empty($section_uids) ? User::loadMultiple($section_uids) : [];
    $protect_subdirectories = $protected_section->get('protect_subdirectories');
    $index_search = $protected_section->get('index_search');

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $protected_section->label(),
      '#description' => $this->t('Label for the protected section.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $protected_section->id(),
      '#machine_name' => [
        'exists' => '\Drupal\section_protection\Entity\ProtectedSection::load',
      ],
      '#disabled' => !$protected_section->isNew(),
    ];

    $form['section_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Section path'),
      '#maxlength' => 255,
      '#default_value' => $protected_section->get('section_path'),
      '#description' => $this->t('The URL path to protect.'),
      '#required' => TRUE,
    ];

    $form['protect_subdirectories'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Protect subdirectories'),
      '#default_value' => is_null($protect_subdirectories) ? TRUE : $protect_subdirectories,
      '#description' => $this->t('Protect all subdirectories of this path.'),
    ];

    $form['index_search'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Index search'),
      '#default_value' => is_null($index_search) ? FALSE : $index_search,
      '#description' => $this->t('By default search is disabled for protected sections, you can enable this functionality here.'),
    ];

    $form['response_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Response'),
      '#default_value' => $protected_section->get('response_type'),
      '#options' => [
        '403' => $this->t('Access denied'),
        '404' => $this->t('Not found'),
        '302' => $this->t('Redirect'),
      ],
      '#description' => $this->t('Select the response triggered when a user does not have access to the section.'),
    ];

    $form['response_redirect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect path'),
      '#maxlength' => 255,
      '#default_value' => $protected_section->get('response_redirect'),
      '#states' => [
        'visible' => [
          'select[name="response_type"]' => ['value' => '302'],
        ],
        'required' => [
          'select[name="response_type"]' => ['value' => '302'],
        ],
      ],
      '#description' => $this->t('Enter the redirect path. Do not redirect to a page in a protected section or you could potentially create a redirect loop.'),
    ];

    $form['section_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#options' => user_role_names(),
      '#description' => $this->t('Select the roles that can access this section. If none are selected everyone can access.'),
    ];
    if (!empty($protected_section->get('section_roles'))) {
      $form['section_roles']['#default_value'] = $protected_section->get('section_roles');
    }
    $form['section_users'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Users'),
      '#target_type' => 'user',
      '#default_value' => $section_users,
      '#tags' => TRUE,
      '#selection_settings' => [
        'include_anonymous' => FALSE,
        'match_operator' => 'CONTAINS',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $validate_path_fields = [
      'section_path',
      'response_redirect',
    ];

    foreach ($validate_path_fields as $field) {
      $path = $form_state->getValue($field);

      if (!empty($path)) {
        if (strpos($path, '/') !== 0) {
          $form_state->setErrorByName($field, $this->t('The path should start with a forward slash (/).'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $protected_section = $this->entity;
    $status = $protected_section->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Protected section.', [
          '%label' => $protected_section->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Protected section.', [
          '%label' => $protected_section->label(),
        ]));
    }
    $form_state->setRedirectUrl($protected_section->toUrl('collection'));
  }

}
