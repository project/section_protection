<?php

namespace Drupal\section_protection\Plugin\search_api\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Processor\ProcessorPluginBase;

/**
 * Filters out users based on their role.
 *
 * @SearchApiProcessor(
 *   id = "protected_section_filter_path",
 *   label = @Translation("Protected section filter by path"),
 *   description = @Translation("Filters out content by path."),
 *   stages = {
 *     "alter_items" = 0,
 *   },
 * )
 */
class ProtectedSectionFilterByPath extends ProcessorPluginBase implements PluginFormInterface {
  use PluginFormTrait;
  /**
   * Can only be enabled for an index that indexes the user entity.
   *
   * {@inheritdoc}
   */
  public static function supportsIndex(IndexInterface $index) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items) {
    // Get protected section data.
    $protected_sections = \Drupal::service('section_protection.protection_tools')->getSectionProtectionEntities();
    $data = \Drupal::service('section_protection.protection_tools')->getSectionProtectionEntitiesUrl();

    // Alter urls returned to remove urls that we want included in search.
    foreach ($protected_sections as $protected_section) {
      if ($protected_section->get('index_search') === TRUE) {
        unset($data[$protected_section->section_path]);
      }
    }

    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item_id => $item) {
      $entity_url = $item->getField('url')->getValues()[0];
      foreach ($data as $value) {
        if (strpos($entity_url, $value) !== FALSE) {
          unset($items[$item_id]);
        }
      }
    }
  }

}
