<?php

namespace Drupal\section_protection\Plugin\search_api\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Processor\ProcessorPluginBase;

/**
 * Filters out users based on their role.
 *
 * @SearchApiProcessor(
 *   id = "protected_section_filter_id",
 *   label = @Translation("Protected section filter by ID"),
 *   description = @Translation("Filters out content by ID."),
 *   stages = {
 *     "alter_items" = 0,
 *   },
 * )
 */
class ProtectedSectionFilterById extends ProcessorPluginBase implements PluginFormInterface {
  use PluginFormTrait;
  /**
   * Can only be enabled for an index that indexes the user entity.
   *
   * {@inheritdoc}
   */
  public static function supportsIndex(IndexInterface $index) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items) {
    // Get protected section data.
    $protected_sections = \Drupal::service('section_protection.protection_tools')->getSectionProtectionEntities();
    $data = \Drupal::service('section_protection.protection_tools')->getSectionProtectionEntitiesId();

    // Alter urls returned to remove urls that we want included in search.
    foreach ($protected_sections as $protected_section) {
      if ($protected_section->get('index_search') === TRUE) {
        $link = $protected_section->get('section_path');
        $path = $this->aliasManager->getPathByAlias($link);
        if (preg_match('/(\D+)\/(\d+)/', $path, $matches)) {
          $entity_type = str_replace('/', '', $matches[1]);
          unset($data[$entity_type . ':' . $matches[2]]);
        }
      }
    }

    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item_id => $item) {
      $entity_type_id = $item->getDatasource()->getEntityTypeId();
      $entity_id = $item->getId();
      $id = $entity_type_id . ':' . (int) filter_var($entity_id, FILTER_SANITIZE_NUMBER_INT);
      $key = array_search($id, $data);
      if (!empty($data) && !empty($id) && $key !== FALSE) {
        unset($items[$item_id]);
        unset($data[$key]);
      }
    }
  }

}
