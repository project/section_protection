<?php

namespace Drupal\section_protection\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Path\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProtectionToolsService.
 *
 * Provides a tools for the Section protection.
 */
class ProtectionToolsService implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  protected $aliasManager;

  /**
   * {@inheritdoc}
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(AliasManagerInterface $alias_manager, EntityTypeManager $entity_type_manager) {
    $this->aliasManager = $alias_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container){
    return new static(
      $container->get('path.alias_manager'),
      $container->get('path.current')
    );
  }

  /**
   * Helper function to get items from Protected section.
   */
  public function getSectionProtectionEntities() {
    return $this->entityTypeManager->getStorage('protected_section')->loadMultiple();
  }

  /**
   * Helper function to get item's ids from Protected section.
   */
  public function getSectionProtectionEntitiesId() {
    $data = [];
    $cid = 'protected_sections:entities_id';
    $list_entities = array_keys(\Drupal::entityTypeManager()->getDefinitions());
    if ($data_cached = \Drupal::cache()->get($cid)) {
      $data = $data_cached->data;
    }
    else {
      $protected_sections = $this->getSectionProtectionEntities();
      foreach ($protected_sections as $protected_section) {
        $link = $protected_section->get('section_path');
        $path = $this->aliasManager->getPathByAlias($link);
        if (preg_match('/(\D+)\/(\d+)/', $path, $matches)) {
          $entity_type = str_replace('/', '', $matches[1]);
          if (in_array($entity_type, $list_entities)) {
            $data[$entity_type . ':' . $matches[2]] = $entity_type . ':' . $matches[2];
          }
        }
      }
      if (!empty($data)) {
        \Drupal::cache()->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, $data);
      }
    }
    return $data;
  }

  /**
   * Helper function to get the item's urls from Protected section.
   */
  public function getSectionProtectionEntitiesUrl() {
    $data = [];
    $cid = 'protected_sections:entities_path';
    if ($data_cached = \Drupal::cache()->get($cid)) {
      $data = $data_cached->data;
    }
    else {
      $protected_sections = $this->getSectionProtectionEntities();
      foreach ($protected_sections as $protected_section) {
        $data[$protected_section->section_path] = $protected_section->section_path;
      }
    }
    if (!empty($data)) {
      \Drupal::cache()->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, $data);
    }
    return $data;
  }

}
