<?php

namespace Drupal\section_protection\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Protected section entities.
 */
interface ProtectedSectionInterface extends ConfigEntityInterface {

}
