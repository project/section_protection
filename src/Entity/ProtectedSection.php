<?php

namespace Drupal\section_protection\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Protected section entity.
 *
 * @ConfigEntityType(
 *   id = "protected_section",
 *   label = @Translation("Protected section"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\section_protection\ProtectedSectionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\section_protection\Form\ProtectedSectionForm",
 *       "edit" = "Drupal\section_protection\Form\ProtectedSectionForm",
 *       "delete" = "Drupal\section_protection\Form\ProtectedSectionDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "protected_section",
 *   admin_permission = "administer section protection",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/workflow/section-protection/{protected_section}",
 *     "add-form" = "/admin/config/workflow/section-protection/add",
 *     "edit-form" = "/admin/config/workflow/section-protection/{protected_section}/edit",
 *     "delete-form" = "/admin/config/workflow/section-protection/{protected_section}/delete",
 *     "collection" = "/admin/config/workflow/section-protection"
 *   }
 * )
 */
class ProtectedSection extends ConfigEntityBase implements ProtectedSectionInterface {

  /**
   * The protected section ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The protected section label.
   *
   * @var string
   */
  protected $label;

  /**
   * The protected section path.
   *
   * @var string
   */
  public $section_path;

  /**
   * Protect section subdirectories.
   *
   * @var boolean
   */
  public $protect_subdirectories;

  /**
   * Response type.
   *
   * @var string
   */
  public $response_type;

  /**
   * Response redirect path.
   *
   * @var string.
   */
  public $response_redirect;

  /**
   * The protected section roles.
   *
   * @var string
   */
  public $section_roles;

  /**
   * The protected section users.
   *
   * @var string
   */
  public $section_users;

  /**
   * Protect section subdirectories.
   *
   * @var boolean
   */
  public $index_search;

}
