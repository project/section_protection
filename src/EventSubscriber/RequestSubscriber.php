<?php

namespace Drupal\section_protection\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;
use Drupal\section_protection\Entity\ProtectedSection;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Path\PathMatcherInterface;

/**
 * Event Subscriber MyEventSubscriber.
 */
class RequestSubscriber implements EventSubscriberInterface {

  /**
   * Protected section entity storage.
   *
   * @var \Drupal\section_protection\Entity\ProtectedSectionInterface
   */
  protected $protectedSectionStorage;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Path Matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * RequestSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user, PathMatcherInterface $path_matcher) {
    $this->protectedSectionStorage = $entity_type_manager->getStorage('protected_section');
    $this->currentUser = $current_user->getAccount();
    $this->pathMatcher = $path_matcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('path.matcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onRequest'];
    return $events;
  }

  /**
   * Handler for kernel request events.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Symfony HTTP kernel event.
   */
  public function onRequest(GetResponseEvent $event) {
    $account = $this->currentUser;

    // Check if account has permission to access all sections.
    if ($account->hasPermission('bypass section protection access')) {
      return;
    }

    $request = $event->getRequest();
    $request_path = $request->getPathInfo();

    // Load all protected section configuration entities being sure to sort them
    // properly.
    $section_ids = $this->protectedSectionStorage->getQuery()
      ->sort('weight', 'ASC')
      ->execute();
    $sections = $this->protectedSectionStorage->loadMultiple($section_ids);

    // Iterate protected section configuration entities.
    foreach ($sections as $section) {
      /* @var \Drupal\section_protection\Entity\ProtectedSection $section */
      $section_path = $section->section_path;

      // Protect subdirectories is selected.
      if ($section->protect_subdirectories) {
        $section_path .= "\n" . $section_path . '/*';
      }

      // Check if the current request path matches a configured section.
      if ($this->pathMatcher->matchPath($request_path, $section_path)) {
        // Handle response if user does not have access to the section.
        if (!$this->hasSectionAccess($account, $section)) {
          $this->handleAccessDenied($section, $event);
        }

        // The first matched path wins so everybody can go home now.
        return;
      }
    }
  }

  /**
   * Handles the response when a user is denied access to a section.
   *
   * @param \Drupal\section_protection\Entity\ProtectedSection $section
   *   A protected section entity.
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Symfony HTTP kernel event.
   */
  protected function handleAccessDenied(ProtectedSection $section, GetResponseEvent $event) {
    switch ($section->response_type) {
      case '404':
        $redirect = Url::fromRoute('system.404')->toString();
        break;

      case '302':
        $path = \Drupal::request()->getRequestUri();
        $redirect = Url::fromUserInput($section->response_redirect, array('query' => array('destination' => $path)))->toString();
        break;

      default:
        $redirect = Url::fromRoute('system.403')->toString();
    }

    $event->setResponse(new RedirectResponse($redirect, '302'));
  }

  /**
   * Checks an account's access to a protected section.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   A user account.
   * @param \Drupal\section_protection\Entity\ProtectedSection $section
   *   A protected section entity.
   *
   * @return bool
   *   TRUE if the account can access the section, FALSE if not.
   */
  protected function hasSectionAccess(AccountInterface $account, ProtectedSection $section) {
    $account_roles = $account->getRoles();
    $section_roles = $this->getSectionRoleIds($section);
    $section_uids = $this->getSectionAccountIds($section);

    // No roles set and no users set. All can access.
    if (!$section_roles && !$section_uids) {
      return TRUE;
    }

    // Accounts with a role in the configured roles can access.
    if ($section_roles && !empty(array_intersect($account_roles, $section_roles))) {
      return TRUE;
    }

    // Accounts that have been explicitly allowed can access.
    if ($section_uids && in_array($account->id(), $section_uids)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Gets role IDs that are allowed to access the protected section.
   *
   * @param \Drupal\section_protection\Entity\ProtectedSection $section
   *   A protected section entity.
   *
   * @return array|bool
   *   An array of role IDs set for the section or FALSE.
   */
  protected function getSectionRoleIds(ProtectedSection $section) {
    $section_roles = array_filter($section->section_roles);

    return !empty($section_roles) ? $section_roles : FALSE;
  }

  /**
   * Gets account IDs that are allowed to access the protected section.
   *
   * @param \Drupal\section_protection\Entity\ProtectedSection $section
   *   A protected section entity.
   *
   * @return array|bool
   *   An array of account IDs set for the section or FALSE.
   */
  protected function getSectionAccountIds(ProtectedSection $section) {
    if (is_array($section->section_users)) {
      $section_uids = array_column($section->section_users, 'target_id');
      return !empty($section_uids) ? $section_uids : FALSE;
    }
    return false;
  }

}
