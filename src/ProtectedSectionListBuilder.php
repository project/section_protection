<?php

namespace Drupal\section_protection;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a listing of Protected section entities.
 */
class ProtectedSectionListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'section_protection_overview';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $help_markup = '<div id="help-text"><p>';
    $help_markup .= $this->t("The sort order of sections is important, especially if you have nested sections. Protection settings for a path will be employed for the first path pattern that is matched. For example: If you have the path /foo protected and set to protect it's subdirectories, then /foo/bar will also be protected with those settings. If you want to have a different settings for /foo/bar and create a protected section for it, it should be placed above /foo in the list. Otherwise the settings for /foo will always take precedent.");
    $help_markup .= '</p></div>';

    $form['help'] = [
      '#markup' => $help_markup,
      '#allowed_tags' => ['div', 'p'],
      '#weight' => -99,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['section_path'] = $this->t('Section');
    $header['section_roles'] = $this->t('Roles');
    $header['section_users'] = $this->t('Users');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\section_protection\Entity\ProtectedSection $entity */
    $row['label'] = $entity->label();
    $row['section_path']['#markup'] = $entity->section_path;
    $section_roles_count = !empty($entity->section_roles) ? count(array_filter($entity->section_roles)) : 0;
    $row['section_roles']['#markup'] = $this->t('Roles (@roles)', ['@roles' => $section_roles_count]);
    $section_users_count = !empty($entity->section_users) ? count($entity->section_users) : 0;
    $row['section_users']['#markup'] = $this->t('Users (@users)', ['@users' => $section_users_count]);

    return $row + parent::buildRow($entity);
  }

}
